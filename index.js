"use strict";

const formidable = require('formidable');
const objectAssign = require('object-assign');

const defaults = Object.freeze({
  allowFiles: false,
  fieldsKey: 'fields',
  filesKey: 'files',
  formidable: Object.freeze({
    multiples: true,
    keepExtensions: true,
    maxFields: 10
  })
});

function KoaSuperBody(opts) {
  const options = objectAssign({}, defaults, opts || {});
  options.formidable = objectAssign({}, defaults.formidable, (opts || {}).formidable);

  return function* koaSuperBodyMain(next) {
    if (typeof this.request.body !== 'undefined' ||
        this.request.method === 'GET') {
      yield* next;
      return;
    }

    let body = yield handleRequest(this, options);
    this.request.body = {};
    this.request.body[options.fieldsKey] = body.fields;
    if (options.allowFiles) {
      this.request.body[options.filesKey] = body.files;
    }

    yield* next;
  };
}

function handleRequest(ctx, opts) {
  let form = new formidable.IncomingForm();
  objectAssign(form, opts.formidable);

  // ignore files when not allowed
  if (!opts.allowFiles) {
    form.onPart = function (part) {
      if (!part.filename) {
        form.handlePart(part);
      }
    };
  }

  let fields = {};
  form.on('field', function (name, value) {
    if (name.endsWith('[]')) {
      name = name.slice(0, -2);
    }
    if (fields.hasOwnProperty(name)) {
      if (!(fields[name] instanceof Array)) {
        fields[name] = [fields[name]];
      }
      fields[name].push(value);
    } else {
      fields[name] = value;
    }
  });

  let files = {};
  form.on('file', function (name, file) {
    if (files.hasOwnProperty(name)) {
      if (!(files[name] instanceof Array)) {
        files[name] = [files[name]];
      }
      files[name].push(file);
    } else {
      files[name] = file;
    }
  });

  return new Promise(function (resolve, reject) {
    form.on('end', function () {
      resolve({ fields: fields, files: files });
    });
    form.on('error', reject);
    form.parse(ctx.req);
  });
}

exports.KoaSuperBody = KoaSuperBody;
